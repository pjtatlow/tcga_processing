#!/bin/bash

#SBATCH --time=12:00:00   # walltime
#SBATCH --ntasks=1   # number of processor cores (i.e. tasks)
#SBATCH --nodes=1   # number of nodes
#SBATCH --mem-per-cpu=65536M   # memory per CPU core
#SBATCH -J "pool_results"   # job name

# LOAD MODULES, INSERT CODE, AND RUN YOUR PROGRAMS HERE


module add r/3.2.1
echo "Starting pool...."
date
Rscript code/pool_logs.R star/
#Rscript code/pool_results.R star/
#python /fslgroup/fslg_GermVar/compute/RNASeq/Temp/code/pool_results.py "star/03*/SJ.out.geneAnnotated.bed" star/totalMappedReads.tab star/averageLength.tab  star/out.tab star/rpkm2.tab
python /fslgroup/fslg_GermVar/compute/RNASeq/Temp/code/pool_results.py "star/*/SJ.out.geneAnnotated.bed" star/totalMappedReads.tab star/averageLength.tab  star/readCounts_final.tab star/rpkm_final.tab

echo "Finished!"
date
