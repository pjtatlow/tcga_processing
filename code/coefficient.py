import sys, glob, os, ast
from utilities import *
from scipy import stats

matrixPath = sys.argv[1]
outDir = sys.argv[2]
type = sys.argv[3]

matrix_kallisto = readMatrixFromFile("%s/Transcript_%s_Kallisto.txt" % (matrixPath, type))
matrix_salmon = readMatrixFromFile("%s/Transcript_%s_Salmon.txt" % (matrixPath, type))
valid = True;
if matrix_salmon[0] != matrix_kallisto[0]:
    valid = False
if valid:
    for x in xrange(len(matrix_kallisto)):
        if matrix_salmon[x][0] != matrix_kallisto[x][0]:
	    valid = False

if valid:
    print "Matricies are valid. Calculating coefficients."
    matrix_kallisto.pop(0)
    for index in range(len(matrix_kallisto)):
        matrix_kallisto[index].pop(0)
        for index2 in range(len(matrix_kallisto[index])):
           matrix_kallisto[index][index2] = ast.literal_eval(matrix_kallisto[index][index2])
    
    matrix_salmon.pop(0)
#    matrix_salmon.pop(len(matrix_salmon) - 1)
    for index in range(len(matrix_salmon)):
        matrix_salmon[index].pop(0)
        for index2 in range(len(matrix_salmon[index])):
            matrix_salmon[index][index2] = ast.literal_eval(matrix_salmon[index][index2])
    
    matrix_kallisto = numpy.array(matrix_kallisto)
    matrix_salmon = numpy.array(matrix_salmon)
    
    print "KALLISTO LENGTH: %d" % len(matrix_kallisto)
    print "SALMON LENGTH: %d" % len(matrix_salmon)
    
    spearman = []
    pearson = []
    
    for x in xrange(len(matrix_kallisto[0])):
        kallisto_column = []
        salmon_column = []
    
        for index in xrange(len(matrix_kallisto)):
        	kallisto_column.append(matrix_kallisto[index][x])
    	        salmon_column.append(matrix_salmon[index][x])
        spearman.append(stats.spearmanr(kallisto_column, salmon_column))
        pearson.append(stats.pearsonr(kallisto_column, salmon_column))
    
    spearman_out = open("%s/Spearman_%s.txt" % (outDir, type),'w')
    pearson_out = open("%s/Pearson_%s.txt" % (outDir, type),'w')
    spearman_out.truncate()
    pearson_out.truncate()
    for x in xrange(len(spearman)):
        spearman_out.write(str(spearman[x][0]))
        pearson_out.write(str(pearson[x][0]))
        spearman_out.write("\t")
        pearson_out.write("\t")
    
    spearman_out.close()
    pearson_out.close()

else :
    print "Matricies are not aligned properly"
