import os, sys, glob, math

inFilePattern = sys.argv[1]
totalMappedReadsFilePath = sys.argv[2]
averageLengthFilePath = sys.argv[3]
outCountsFilePath = sys.argv[4]
outRpkmFilePath = sys.argv[5]

inFilePaths = glob.glob(inFilePattern)

print "Pooling data..."

metaDict = {}
countDict = {}
headerItems = None
samples = []
for inFilePath in inFilePaths:
    inFileData = [line.rstrip().split("\t") for line in file(inFilePath)]
    sampleID = os.path.basename(os.path.dirname(inFilePath))

    if inFilePath == inFilePaths[0]:
        headerItems = inFileData.pop(0)
        headerItems = headerItems[:4] + headerItems[5:]
    else:
        inFileData.pop(0)

    #headerItems.append(sampleID)
    samples.append(sampleID)
    for rowItems in inFileData:
        metaItems = rowItems[:4] + rowItems[5:]
        rowID = metaItems[0]
        counts = rowItems[4]

	if not rowID in metaDict:
	    metaDict[rowID] = metaItems

        if not rowID in countDict:
            countDict[rowID] = {}

	countDict[rowID][sampleID] = counts

for item in sorted(samples):
    headerItems.append(item)

totalMappedReads = {}
for line in file(totalMappedReadsFilePath):
    lineitems = line.rstrip().split("\t")
    totalMappedReads[lineitems[0]] = lineitems[1]

averageLength = {}

for line in file(averageLengthFilePath):
    lineitems = line.rstrip().split("\t")
    averageLength[lineitems[0]] = lineitems[1]

#print "030809ad-2009-424d-be47-7184c72393e1: " + totalMappedReads["030809ad-2009-424d-be47-7184c72393e1"]
#print "030868b4-2b46-4897-a03c-ec60c2bb71a5: " + totalMappedReads["030868b4-2b46-4897-a03c-ec60c2bb71a5"]
#print "0324c3ce-3e7a-4ecd-87ef-3d02b591c0b0: " + totalMappedReads["0324c3ce-3e7a-4ecd-87ef-3d02b591c0b0"]
#print "03c2f6ba-caef-41e1-ad3e-7a0a76d9f4f7: " + totalMappedReads["03c2f6ba-caef-41e1-ad3e-7a0a76d9f4f7"]



outCountsFile = open(outCountsFilePath, 'w')
outCountsFile.write("\t".join(headerItems) + "\n")
outRpkmFile = open(outRpkmFilePath, 'w')
outRpkmFile.write("\t".join(headerItems) + "\n")

print "There are %d genes to calculate" % len(metaDict)
print ""
i = 1
for rowID in sorted(metaDict):
    print "\rCurrently on: %d" % i,
    outCountsRow = list(metaDict[rowID])
    outRpkmRow = list(metaDict[rowID])
    for sampleID in headerItems[8:]:
        sampleCount = "0"
        rpkm = "0"
        if sampleID in countDict[rowID]:
            sampleCount = countDict[rowID][sampleID]
            tmr = float(totalMappedReads[sampleID])
            al = float(averageLength[sampleID])
            rpkm = "%.9f" % ((math.pow(10,9) * float(sampleCount)) / (tmr * al))
        outCountsRow.append(sampleCount)
        outRpkmRow.append(rpkm)
#        print sampleCount
#        print str(tmr)
#        print str(al)
#        sys.exit()
    outCountsFile.write("\t".join(outCountsRow) + "\n")
    outRpkmFile.write("\t".join(outRpkmRow) + "\n")
    i += 1
outCountsFile.close()
outRpkmFile.close()
