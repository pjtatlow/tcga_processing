#arguments in order path_to_kallisto_file, path_to_salmon_file, path_to_pdf
args <- commandArgs(trailingOnly = TRUE)

data_kallisto <- read.table(args[1], check.names = FALSE)
data_salmon <- read.table(args[2], check.names = FALSE)

data_kallisto <- sapply(data_kallisto, as.numeric)
data_salmon <- sapply(data_salmon, as.numeric)
spearman <- c(1:ncol(data_kallisto));
for(i in 1:ncol(data_kallisto)) {
  spearman[i] <- cor(data_kallisto[,i],data_salmon[,i],method = "spearman")
}
highest_3 <- sort(spearman, decreasing = TRUE, index.return = TRUE)$ix[1:3]
lowest_3 <- sort(spearman, decreasing = FALSE, index.return = TRUE)$ix[1:3]
to_plot = c(highest_3,lowest_3)

pdf(args[3])
for (i in to_plot) {
  plot(log2(data_kallisto[,i]+1), log2(data_salmon[,i]+1),main = colnames(data_salmon)[i],sub = spearman[i],xlab = "Kallisto",ylab="Salmon")
}
dev.off()

    
