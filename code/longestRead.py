import os,sys

file = sys.argv[1]

longest_line = 0;
with open(file,'r') as f:
    for line in f:
    	length = int(line)
	if length > longest_line:
	    longest_line = length

print "Longest read is %d nucleotides long" % longest_line
