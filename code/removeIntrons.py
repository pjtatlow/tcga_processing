import os,sys

in_file = sys.argv[1]
out_file = sys.argv[2]
out = open(out_file, 'w')

def validChr(string):
	values = string.split("\t")
	if '_' in values[0]:
		return False
	else:
		return True

longest_line = 0;
with open(in_file,'r') as f:
	for line in f:
		if validChr(line):
			out.write(line)
		else:
			print "Found invalid line: " + line
out.close()
