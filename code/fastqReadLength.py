import os,sys,glob

path = sys.argv[1]

longest_line = 0;
for file in glob.glob(os.path.join(path, "*.fastq")):
    with open(file,'r') as f:
    	for line in f:
            if not line[0] == "@" and not line[0] == "+":
	        length = len(line)
	        if length > longest_line:
	            longest_line = length

print longest_line
