import os, sys, glob
from utilities import *

salmonDirPath = sys.argv[1]

if not os.path.exists("%s/FPKM" % salmonDirPath):
    os.mkdir("%s/TPM" % salmonDirPath)
    os.mkdir("%s/FPKM" % salmonDirPath)
    os.mkdir("%s/Counts" % salmonDirPath)

ids = {}

idFiles = glob.glob("SampleIDMap/*")

for file in idFiles:
    with open(file,'r') as f:
	file_name = os.path.basename(file)
        first_line = f.readline()
        first_line = first_line.rstrip()
	ids[file_name] = first_line        

salmonFilePaths = glob.glob("%s/*/quant.sf" % salmonDirPath)

for inFilePath in salmonFilePaths:
    longSampleID = os.path.basename(os.path.dirname(inFilePath))
    sampleID = ids[longSampleID]
    outTpmFilePath = "%s/TPM/%s" % (salmonDirPath, sampleID)
    outFpkmFilePath = "%s/FPKM/%s" % (salmonDirPath, sampleID)
    outCountFilePath = "%s/Counts/%s" % (salmonDirPath, sampleID)

    if not os.path.exists(outCountFilePath):
        print "Parsing values from %s" % inFilePath
        data = [x for x in readMatrixFromFile(inFilePath) if not x[0].startswith("Name")]

        tpmData = [[x[0], x[3]] for x in data]
        fpkmData = [[x[0], x[3]] for x in data]
        countData = [[x[0], x[4]] for x in data]

        writeMatrixToFile(tpmData, outTpmFilePath)
        writeMatrixToFile(fpkmData, outFpkmFilePath)
        writeMatrixToFile(countData, outCountFilePath)
