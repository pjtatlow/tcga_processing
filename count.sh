#!/bin/bash

#SBATCH --time=24:00:00   # walltime
#SBATCH --ntasks=2   # number of processor cores (i.e. tasks)
#SBATCH --nodes=1   # number of nodes
#SBATCH --mem-per-cpu=4096M   # memory per CPU core
#SBATCH -J "process_files"   # job name

# LOAD MODULES, INSERT CODE, AND RUN YOUR PROGRAMS HERE
echo "Starting time:"
date

python code/fastqReadLength.py $1 >> count/counts.txt


echo "Ending time:"
date
