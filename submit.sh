#!/bin/bash

for d in FASTQ/*; do
	basename=$(basename $d)
	if [ -f stdout/star/$basename ]; then
		echo "Skipping $d. Seems to exist already"
	else
		echo "Running $d."
		sbatch process.sh $d	
	fi
done
