#!/bin/bash

#SBATCH --time=02:00:00   # walltime
#SBATCH --ntasks=24   # number of processor cores (i.e. tasks)
#SBATCH --nodes=1   # number of nodes
#SBATCH --mem-per-cpu=2048M   # memory per CPU core
#SBATCH -J "process_files"   # job name

# LOAD MODULES, INSERT CODE, AND RUN YOUR PROGRAMS HERE


echo "Starting time:"
date
echo "Directory is: $1"
module load r/3.2.1
  if [[ -d $1 ]]; then
	num_files=$(ls $1 | wc -l)
	sample=$(basename $1)
	io_dir='/tmp/'
	#io_dir='star/'
	mkdir -p $io_dir$sample
	echo "BATCH ID: $sample"

	if [[ $num_files == '1' ]]; then
		file1=$(ls $1/*.fastq.gz)
		echo "Running Star for $1 (single-end reads)"
		{ time star --genomeDir genome/star_index -- readFilesIn $file1 --runThreadN 16 --outFilterMismatchNoverLmax 0.05 --outSAMattributes All --outSJfilterOverhangMin 20 10 10 10 --outFileNamePrefix $(echo $io_dir)$sample/ --sjdbFileChrStartEnd genome/introns_refseq_hg19.bed --sjdbOverhang 76 --readFilesCommand gunzip -c --outSAMtype BAM SortedByCoordinate --limitBAMsortRAM 40000000000 > stdout/star/$sample 2>&1 ; } 2> timer/star/single/$sample
		#echo "Running Salmon for $1 (single-end reads)"
		#{ time salmon quant -i genome/salmon_index_refseq_sequence_mrna_Nov2014 -l IU -p 8 -r "$file1" -o salmon/$sample > stdout/salmon/$sample 2>&1 ; } 2> timer/salmon/single/$sample
		#echo "Running Kallisto for $1 (single-end reads)"
		#{ time kallisto quant -i genome/kallisto_index_refseq_sequence_mrna_Nov2014 -b 100 -t 8 -l 200 -s 5 --plaintext --bias --single -o kallisto/$sample $file1 > stdout/kallisto/$sample 2>&1 ; } 2> timer/kallisto/single/$sample
		#echo "File 1: $file1"
	elif [[ $num_files == '2' ]]; then
		file1=$(ls $1/*1.fastq.gz)
		file2=$(ls $1/*2.fastq.gz)
		echo "Running Star for $1 (paired-end reads)"
		{ time star --genomeDir genome/star_index -- readFilesIn $file1 $file2 --runThreadN 16 --outFilterMismatchNoverLmax 0.05 --outSAMattributes All --outSJfilterOverhangMin 20 10 10 10 --outFileNamePrefix $(echo $io_dir)$sample/ --sjdbFileChrStartEnd genome/introns_refseq_hg19.bed --sjdbOverhang 76 --readFilesCommand gunzip -c --outSAMtype BAM SortedByCoordinate --limitBAMsortRAM 40000000000 > stdout/star/$sample 2>&1 ; } 2> timer/star/paired/$sample
		#echo "Running Salmon for $1 (paired-end reads)"
		#{ time salmon quant -i genome/salmon_index_refseq_sequence_mrna_Nov2014 -l IU -p 8 -1 "$file1" -2 "$file2" -o salmon/$sample > stdout/salmon/$sample 2>&1 ; } 2> timer/salmon/paired/$sample
		#echo "Running Kallisto for $1 (paired-end reads)"
		#{ time kallisto quant -i genome/kallisto_index_refseq_sequence_mrna_Nov2014 -b 100 -t 8 --plaintext --bias -o kallisto/$sample $file1 $file2 > stdout/kallisto/$sample 2>&1 ; } 2> timer/kallisto/paired/$sample
		#echo "File 1: $file1, File 2: $file2"
		
	else
		echo "Skipping $1. there were $num_files files"
	fi
 fi

echo "Starting post-star processing"

echo "Part1 with $sample... "	
	Rscript /fslgroup/fslg_GermVar/compute/RNASeq/Temp/code/STARtoBED.R "$io_dir"$(echo $sample)/SJ.out.tab "$io_dir"$(echo $sample)/SJ.out.bed "Ending time:"
echo "Part2 with $sample... "
	intersectBed -wao -a "$io_dir"$(echo $sample)/SJ.out.bed -b /fslgroup/fslg_GermVar/compute/RNASeq/Temp/genome/refseq_annotation_hg19.formatted.gtf -s >> "$io_dir"$(echo $sample)/SJ.out.enriched.bed
echo "Part3 with $sample... "
	awk -F"\t" '$17 == "1" { print }' "$io_dir"$(echo $sample)/SJ.out.enriched.bed > "$io_dir"$(echo $sample)/SJ.out.enriched.filtered.bed
	awk '{print $4,$17}' "$io_dir"$(echo $sample)/SJ.out.enriched.bed | sort -u > "$io_dir"$(echo $sample)/SJ.out.enriched.unique.bed
	sed -i -e 's/ 0/ "";/g' "$io_dir"$(echo $sample)/SJ.out.enriched.unique.bed
	Rscript /fslgroup/fslg_GermVar/compute/RNASeq/Temp/code/GenestoJunctions.R "$io_dir"$(echo $sample)/SJ.out.bed "$io_dir"$(echo $sample)/SJ.out.enriched.unique.bed "$io_dir"$(echo $sample)/SJ.out.enriched.filtered.bed "$io_dir"$(echo $sample)/SJ.out.geneAnnotated.bed
	#Rscript /fslgroup/fslg_GermVar/compute/RNASeq/Temp/code/pool_results.R "$io_dir"

mkdir -p ~/working/star/$sample
cp $(echo $io_dir)$sample/Log.final.out $(echo $io_dir)$sample/SJ.out.geneAnnotated.bed ~/working/star/$sample/
rm -rvf /tmp/

date
