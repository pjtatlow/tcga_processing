#!/bin/bash

#SBATCH --time=96:00:00   # walltime
#SBATCH --ntasks=1   # number of processor cores (i.e. tasks)
#SBATCH --nodes=1   # number of nodes
#SBATCH --mem-per-cpu=1024M   # memory per CPU core
#SBATCH -J "gzip_fastq"   # job name
#SBATCH --mail-user=PJ.Tatlow@gmail.com   # email address
#SBATCH --mail-type=END
#SBATCH --mail-type=FAIL

for i in $1/[ef]*/*.fastq
do
echo "Zipping $i"
gzip $i

done
